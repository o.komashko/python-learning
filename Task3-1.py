'''Create a function that checks that the number given in the argument is
prime. The function should take a numeric value and return a logical
value of True / False.
E.g.
For 11 the function will return True, for 12 -> False.'''

def given_number(number): # primer numbr can't be 0 and 1. It should be <2

    if number > 1:
        for i in range(2,number):
            if (number % i) == 0:
                return False
        return True
    else:
        return False

x1 = int(input('Enter the number and system will check is it prime or not: '))
print(given_number(x1))

