"""Create a Rectangle class whose attributes will be the height and width of
the figure. Implement methods to measure the perimeter and area of a
rectangle.
Then create a Square class, remembering that every square is a
rectangle, but not every rectangle is a square."""

class Rectangle:
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def rectangles_perimeter(self):
        return 2 * (self.width + self.height)

    def rectangle_area(self):
        return self.width * self.height

    def __str__(self):
        return f'({self.rectangle_area()})'

class Square(Rectangle):
    def __init__(self, width, height):
        super().__init__(width, height)

    def square_area(self):
        return self.width * self.height

    def __str__(self):
        return f'({self.square_area()})'

x = Rectangle(3,4)
y = Square(4,4)
#print(x.rectangles_perimeter())
print(x)
print(y)