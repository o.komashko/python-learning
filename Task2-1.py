'''Create a function that takes a list of integers and returns what the
smallest number is in.
Do not use built-in functions.
eg. for the list [42, 13, 56, 7, 12, 3, 85] the function should return 3, because
the smallest element is found at this index in this list.'''

def integers_list(numbers):
    smallest_no = numbers[0]

    for x in numbers:
        if x < smallest_no:
            smallest_no = x
    return smallest_no

smallest_element = integers_list([42, 13, 56, 7, 12, 3, 85])
print(f'Smallest element is {smallest_element}')

# new_element = integers_list([34, 2, 7, 92, 4])
# print(f'Smallest element is {new_element}')