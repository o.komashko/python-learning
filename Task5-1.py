'''Create a function that will calculate the number of upper and
lower case
letters in a string.
eg for: "The quick Brown Fox"
expected result:
Number of uppercase letters: 3, number of lowercase letters : 13
Hint: use a loop, conditional statement, and appropriate methods for the
string.'''

my_string = input('Enter a string: ') # A string is defined and is displayed on the console.
counter_upper = 0 # A counter value is initialized to 0. Kuna lause/string loetelu algab 0-st.
counter_lower = 0 # A counter value is initialized to 0.
for i in my_string: # The string is iterated over, and checked to see if it contains upper
                    # case alphabets using the ‘isupper’ method.
    if (i.isupper()):
        counter_upper = counter_upper + 1 # If so, the counter is incremented by 1 until
                                    # the end of the string.
    elif (i.islower()): # The string is iterated over, and checked to see if it contains lower
                        # case alphabets using the ‘islower’ method.
        counter_lower = counter_lower +1 # If so, the counter is incremented by 1 until
                                    # the end of the string.

print(f'Number of uppercase letters: {(counter_upper)}, '
      f'number of lowercase letters {(counter_lower)}')




