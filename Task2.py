""" implement a speed limit alert function for drivers in Estonia using their Distance and time.
Note: Formula for Speed = Distance ÷ Time. the program takes as input the distance and time.
if speed limit is between 0 - 20km/h show message "You driving slow"
if speed limit is between 21 - 45km/h show message "You driving with the limit for inner streets"
if speed limit is between 46 - 70km/h show message "You driving faster. watch out for speed bumps"
if speed limit is between 71 - 100km/h show message "You driving very fast. Ensure you use your seat belt"
if speed limit is above 100km/h show message "You are driving recklessly and putting your life at risk" """

def speed_formula(distance, time): #function
    speed = distance / time # formula
    if 0 <= speed >= 20:  # speed >= 0 and speed <= 20
        return ('slow')
    elif 21 <= speed >= 45:  # speed >= 21 and speed <= 45
        return ('with the limit for inner streets')
    elif 46 <= speed >= 70:  # speed >= 46 and speed <= 70
        return ('very fast. Ensure you use your seat belt')
    elif speed >= 71 and speed <= 100:
        return ('You driving very fast. Ensure you use your seat belt')
    else:
        return ('recklessly and putting your life at risk')

user_distance = float(input("Enter distance: "))
user_time = float(input("Enter time: "))

x = speed_formula(user_distance, user_time)
print(f'You driving  {x}')