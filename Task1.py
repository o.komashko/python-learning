# Task 1: Estonia govt gives scholarship only to those with valid name and phone number.

def scholarship(name, phone_number): # function

    if name.isalpha() and len(phone_number) > 10:
        return ('Congratulations you got the scholarship!')
    else:
        return ('You are not qualified.')

user_name = input("Enter your name: ")
user_phone_number = input("Enter your phone number: ")
print(scholarship(user_name, user_phone_number))

