'''Write a program that will convert the sequence of numbers entered by
the user into text, e.g .:
112 -> "one one two"
9973 -> "nine nine seven three"
Hint: you need the input () function, a dictionary and a loop.'''


sequence_of_numbers = input('Please enter the number: ')

my_list = {
    "0": "zero", "1": "one", "2": "two", "3": "three", "4": "four",
    "5": "five", "6": "six", "7": "seven", "8": "eight", "9": "nine"
} # declaring a list

def print_my_list(number):
    string_number = ''       # create a variable
    for element in number:
        string_number += (my_list.get(element) + ' ') #covert integer to string
    return string_number
print(print_my_list(sequence_of_numbers))




