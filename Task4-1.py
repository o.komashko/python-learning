'''Create a function that checks if the string given as an argument is a
palindrome (ie reading backwards and forwards is exactly the same, eg
"kayak", "madam").'''

# string = input('Enter a string: ')
# if (string == string [::-1]):
#     print('The string is a palindrome.')
# else:
#     print('The string is not a palindrome.')

string_word = input('Enter a string: ')
rev = reversed(string_word)
if list(string_word) == list(rev):
    print('It is a palindrome.')
else:
    print('It is not a palindrome.')
