"""Write a function that will return the 5 most common words from
Mickiewicz's work. https://pastebin.com/raw/aAHeW5Pt (copy and save
to a text file what you will find at this link).
Hint: use the open() function, split() method, dictionary and loop.
Autors: Olga and Merli
"""


with open("data_task7.txt", encoding="utf8") as f:
    content = f.read()
    # print(content)
    content = content.lower().split()
    # print(content)
    dict_of_words = {}
    for word in content:
        if word in dict_of_words:
            dict_of_words[word] += 1
        elif word not in dict_of_words:
            dict_of_words[word] = 1
    # print(dict_of_words)

    sorted_dict_of_words = sorted(((value, key) for (key, value) in dict_of_words.items()), reverse=True)
    # print(sorted_dict_of_words)
    sorted_dict_of_words = dict([(key, value) for (value, key) in sorted_dict_of_words])
    # print(sorted_dict_of_words)
    list_of_dict_keys = list(sorted_dict_of_words.keys())
    # print(list_of_dict_keys)
    five_first_list_items = list_of_dict_keys[:5]
    print(five_first_list_items)
