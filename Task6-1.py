'''Write a function that takes two strings and checks to see if they are
anagrams of each other.
For example:
"Army" and "Mary",
"Sharing" and "Sunday",
"Quid est veritas?" and "Vir est qui adest",
"Jim Morrison" and "Mr. Mojo Risin"
"Tom Marvolo Riddle" and "I am Lord Voldemort"'''

def anogram_check(str1,str2):
    if(sorted(str1)) == (sorted(str2)):
        print('Both string are anagrams.')
    else:
        print('Both strings are not anagrams.')

str1 = 'Army'
str2 = 'Mary'
print('String value 1: ', str1)
print('String value 2: ', str2)
anogram_check(str1,str2)


