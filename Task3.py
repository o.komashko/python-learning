# implement a function that takes student score and tells their grade ()

def student_score(completion): #function

        if completion >=91:
            return ('A (Excellent)')
        elif completion >=81:
            return ('B (Very good, with few errors)')
        elif completion >=71:
            return ('C (Good, with some errors)')
        elif completion >=61:
            return ('D (Satisfactory, with many errors)')
        elif completion >=40: # In the link was >=51
            return ('E Sufficient')
        else:
            medical_condition = input("Do you have a medical condition?(yes/no): ")
            if medical_condition == "yes":
                return ('Pass')
            else:
                return ('Fail')


completion = float(input("Enter you score: "))

x = student_score(completion)
print(f'You grade is  {x}')